-- Demo Plugin
--
-- This is a demonstration plugin which will not be installed as part of
-- Gitano.  Its purpose is to show the way that a plugin can add commands
-- to Gitano if it so desires.
--
-- Copyright 2014-2017 Daniel Silverstone <daniel.silverstone@codethink.co.uk>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.

local gitano = require "gitano"

local demo_short_help = "Simple demo command"
local demo_helptext = [[
This is the long help text for the demonstration plugin 'demo' command.

Enjoy.
]]

local function demo_validate(config, repo, cmdline)
   if #cmdline ~= 2 then
      gitano.log.error("usage: demo <reponame>")
      return false
   end
   return true
end

local function demo_prep(config, repo, cmdline, context)
   context.operation = "read"
   return config.repo:run_lace(context)
end

local function demo_run(config, repo, cmdline, env)
   local p = gitano.log.stdout
   p(("Repo is: %s"):format(tostring(repo)))
   for i, n in ipairs(cmdline) do
      p(("cmdline[%d] is: %s"):format(i, tostring(n)))
   end
   for k, v in pairs(env) do
      p(("env[%s] is: %s"):format(k, tostring(v)))
   end
   return "exit", 0
end

assert(gitano.command.register("demo",
                               demo_short_help, demo_helptext,
                               demo_validate, demo_prep, demo_run,
                               true, false, false))
