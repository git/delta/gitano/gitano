-- git-upload-archive plugin
--
-- This plugin adds the `git-upload-archive` command support necessary to
-- permit users to run `git archive` against a URI served by Gitano.
--
-- Copyright 2015-2017 Daniel Silverstone <dsilvers@digital-scurf.org>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.

local gitano = require "gitano"

local function builtin_upload_archive_validate(config, repo, cmdline)
   -- git-upload-archive repo
   if #cmdline > 2 then
      return false
   end
   cmdline[2] = repo:fs_path()
   return true
end

local function builtin_upload_archive_prep(config, repo, cmdline, context)
   if repo.is_nascent then
      return "deny", "Repository " .. repo.name .. " does not exist"
   end
   -- git-upload-archive is always a simple write operation
   context.operation = "read"
   return repo:run_lace(context)
end

local function builtin_upload_archive_run(config, repo, cmdline, env)
   local cmdcopy = {"upload-archive", env=env}
   for i = 2, #cmdline do cmdcopy[i] = cmdline[i] end
   return repo:git_command(cmdcopy)
end

assert(gitano.command.register("git-upload-archive", nil, nil,
                               builtin_upload_archive_validate,
                               builtin_upload_archive_prep,
                               builtin_upload_archive_run,
                               true, true))
