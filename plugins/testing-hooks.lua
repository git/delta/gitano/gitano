-- Plugin for testing hooks
--
-- This is a testing plugin which will not be installed as part of
-- Gitano.  Its purpose is to allow the test suite to verify various parts
-- of the hooking system in Gitano.
--
-- Copyright 2017 Daniel Silverstone <daniel.silverstone@digital-scurf.org>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.

local gitano = require "gitano"

local function _check(what, hookname)
   local aborts = "," .. (os.getenv("HOOK_"..what) or "") .. ","
   return (aborts:find("," .. hookname .. ","))
end

local function _abort(...) return _check("ABORT", ...) end
local function _decline(...) return _check("DECLINE", ...) end

-- This function allows us to verify behaviour of the preauth_cmdline hook
local function preauth_cmdline_hookfunc(cancel, ip, user, keytag, cmd, ...)
   if _abort("PREAUTH_CMDLINE") then
      return nil, "Aborted on request"
   end
   if _decline("PREAUTH_CMDLINE") then
      return "stop", true, "Declined on request"
   end
   if os.getenv("PREAUTH_CMDLINE_REMOVEME") and cmd == "removeme" then
      return "update", cancel, ip, user, keytag, ...
   end
   return "continue"
end

gitano.hooks.add(gitano.hooks.names.PREAUTH_CMDLINE,
		 10, preauth_cmdline_hookfunc)

-- This function is meant to allow us to check that the POST_RECEIVE hook works
local function post_receive_hookfunc(repo, updates)
   if _abort("POST_RECEIVE") then
      return nil, "Aborted on request"
   end
   if _decline("POST_RECEIVE") then
      io.stdout:write("HOOKFUNC_STOPPED")
      io.stdout:flush()
      return "stop"
   end
   return "continue"
end

-- We deliberately install the hook between core behaviour and supple
gitano.hooks.add(gitano.hooks.names.POST_RECEIVE,
		    -100, post_receive_hookfunc)
