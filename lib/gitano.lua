-- gitano
--
-- Top level include for gitano
--
-- Copyright 2012-2017 Daniel Silverstone <dsilvers@digital-scurf.org>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
--

local util = require 'gitano.util'
local config = require 'gitano.config'
local repository = require 'gitano.repository'
local log = require 'gitano.log'
local command = require 'gitano.command'
local actions = require 'gitano.actions'
local lace = require 'gitano.lace'
local supple = require 'gitano.supple'
local auth = require 'gitano.auth'
local plugins = require 'gitano.plugins'
local i18n = require 'gitano.i18n'
local patterns = require 'gitano.patterns'
local hooks = require 'gitano.hooks'

local _VERSION = {1, 3, 0}
_VERSION.major = _VERSION[1]
_VERSION.minor = _VERSION[2]
_VERSION.patch = _VERSION[3]

local _VERSION_STRING = ("Gitano %d.%d.%d"):format(_VERSION.major,
						   _VERSION.minor,
						   _VERSION.patch)

local _COPYRIGHT = "Copyright 2012-2019 Daniel Silverstone <dsilvers@digital-scurf.org>"

return {
   VERSION = _VERSION,
   VERSION_STRING = _VERSION_STRING,
   COPYRIGHT = _COPYRIGHT,
   util = util,
   config = config,
   repository = repository,
   log = log,
   command = command,
   actions = actions,
   lace = lace,
   supple = supple,
   auth = auth,
   plugins = plugins,
   i18n = i18n,
   patterns = patterns,
   hooks = hooks,
}
