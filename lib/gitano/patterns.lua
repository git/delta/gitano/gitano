-- gitano.patterns
--
-- Centralised pattern definitions. Not stable ABI.
--
-- Copyright 2017 Richard Maw <richard.maw@gmail.com>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
--

local _NICE_NAME = "[a-z][a-z0-9_.-]+"

local USER_INFO_PREFIX = "^(users/.-)(".. _NICE_NAME .. ")/"
local GROUP_INFO_PREFIX = "^(groups/.-)(".. _NICE_NAME .. ")"
local KEYRING_INFO_PREFIX = "^(keyrings/.-)(".. _NICE_NAME .. ")"

local CONF_ENDS_WILDCARD = "%.%*$"

local GIT_REPO_SUFFIX = "%.git$"

return {
    TEXT_LINE = "([^\n]*)\n",
    DOTFILE = "^%.",
    VALID_USERNAME = "^" .. _NICE_NAME .. "$",
    VALID_SSHKEYNAME = "^" .. _NICE_NAME .. "$",
    USER_INFO_PREFIX = USER_INFO_PREFIX,
    USER_CONF_MATCH = USER_INFO_PREFIX .. "user%.conf$",
    USER_KEY_MATCH = USER_INFO_PREFIX .. "(" .. _NICE_NAME .. ")%.key$",
    SSH_KEY_CONTENTS = "^([^ ]+) ([^ ]+) ([^ ].*)$",
    VALID_GROUPNAME = "^" .. _NICE_NAME .. "$",
    GROUP_INFO_PREFIX = GROUP_INFO_PREFIX,
    GROUP_CONF_MATCH = GROUP_INFO_PREFIX .. "%.conf$",
    VALID_KEY_FINGERPRINT = "^" .. string.rep("[0-9A-Fa-f]", 40) .. "$",
    VALID_KEYRING_NAME = "^" .. _NICE_NAME .. "$",
    KEYRING_INFO_PREFIX = KEYRING_INFO_PREFIX,
    KEYRING_MATCH = KEYRING_INFO_PREFIX .. "%.gpg$",
    GPG_OUTPUT_FINGERPRINT_MATCH = "fpr:::::::::([0-9A-F]+):",
    HTTP_RESPONSE = "^HTTP/1.[01] (...) ?([^\r\n]+)\r?\n(.-)\r?\n\r?\n(.*)$",
    HTTP_HEADER = "([^:\r\n]+): *([^\r\n]+)",
    CONF_ENDS_WILDCARD = CONF_ENDS_WILDCARD,
    CONF_WILDCARD = "^(.+)" .. CONF_ENDS_WILDCARD,
    CONF_ARRAY_INDEX = "^(.+)%.i_[0-9]+$",
    CONF_SET_TYPE_PREFIX = "^([sbi]):(.*)$",
    GIT_REPO_SUFFIX = GIT_REPO_SUFFIX,
    GIT_REPO_NAME_MATCH = "^(.+)" .. GIT_REPO_SUFFIX,
    LACE_GLOBAL_DEFINITION = "^global:(.+)$",
    PLUGIN_NAME = "^([^_]+)%.lua$",
    REF_IS_NORMALISED = "^refs/",
    PARSE_TIME_AND_TZOFFSET = "^([0-9]+) ([+-][0-9]+)$",
    SUPPLE_MODULE_LOAD_MATCH = "^([^%.]+)%.(.+)$",
    GITHOOK_PARSE_CHANGESET = "([^ ]+) ([^ ]+) ([^\n]+)\n?",
}
