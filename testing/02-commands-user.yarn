<!-- -*- markdown -*- -->

Users
=====

A core concept in Gitano is that of users.  Users have usernames, real names,
email addresses, ssh keys, and membership of groups.  All this can be
administered via the `user` command which is convenient and delegable.

Creating users
==============

    SCENARIO user creation

    GIVEN a standard instance
      AND testinstance, using adminkey, adds a new user alice, with a key called main

By default, normal users may not create users.

    WHEN alice main, expecting failure, runs user add bob bob@testinstance Bob Bobertson
    THEN stderr contains CRIT: You may not perform site administration

Privileged users may create other users though.

    WHEN testinstance adminkey runs user add bob bob@testinstance Bob Bobertson
     AND testinstance adminkey runs user list
    THEN stdout contains ^bob

    FINALLY the instance is torn down

Manipulating user information
=============================

    SCENARIO user manipulation

    GIVEN a standard instance

The e-mail address and real name provided in the create command are also listed.

    WHEN testinstance adminkey runs user add bob bob@testinstance Bob Bobertson
     AND testinstance adminkey runs user list
    THEN stdout contains ^bob.*bob@testinstance
     AND stdout contains ^bob.*Bob Bobertson

The e-mail address can be changed,
which is handy for if the user changes e-mail provider.

    WHEN testinstance adminkey runs user email bob bob@example.com
     AND testinstance adminkey runs user list
    THEN stdout contains ^bob.*bob@example.com

A user's real name may also be changed.

    WHEN testinstance adminkey runs user name bob Robert Robertson
     AND testinstance adminkey runs user list
    THEN stdout contains ^bob.*Robert Robertson

    FINALLY the instance is torn down

Renaming users
==============

Since usernames can be used in rules, it's not something one might do lightly,
but it is possible to rename users.

    SCENARIO rename user
    GIVEN a standard instance

    WHEN testinstance adminkey runs user add bob bob@testinstance Bob Bobertson
     AND testinstance adminkey runs user rename bob rob.ert --force
    THEN the output contains SSH authorised key file updated
     AND the output contains Committed: Rename user bob to rob.ert
    WHEN testinstance adminkey runs user list
    THEN stdout contains ^rob.ert
    THEN stdout does not contain ^bob

In addition, you might want rename a user which owns repositories.  When that
is done, Gitano must re-own the repository in order that rules using the
project's owner work properly.

    WHEN testinstance adminkey runs create testrepo rob.ert
     AND testinstance adminkey runs config testrepo show project.owner
    THEN stdout contains rob.ert

    WHEN testinstance adminkey runs user rename rob.ert b.ob --force
     AND testinstance adminkey runs config testrepo show project.owner
    THEN stdout contains b.ob

    FINALLY the instance is torn down

Renaming Self
=============

It is possible to rename yourself depending on the rules that are set

    SCENARIO rename self
    GIVEN a standard instance
     AND testinstance, using adminkey, adds a new user bob, with a key called bobskey

    WHEN testinstance adminkey runs group adduser gitano-admin bob
     AND bob bobskey runs user rename bob robert --force
    THEN the output contains SSH authorised key file updated
     AND the output contains Committed: Rename user bob to robert
    WHEN testinstance adminkey runs user list
    THEN stdout contains ^robert
     AND stdout does not contain ^bob

    FINALLY the instance is torn down

Deleting users
==============

Removing users from the system is sometimes necessary.  If the user is removed
then they cannot authenticate and thus cannot do anything they were previously
able to do which an anonymous user cannot.

    SCENARIO delete user
    GIVEN a standard instance

    WHEN testinstance adminkey runs user add robert bob@testinstance Bob Bobertson
     AND testinstance adminkey runs user del robert --force
    THEN the output contains SSH authorised key file updated
     AND the output contains Committed: Delete user robert
    WHEN testinstance adminkey runs user list
    THEN stdout does not contain ^robert

    FINALLY the instance is torn down

regression checks
=================

    SCENARIO user add with slashes causes error

    GIVEN a standard instance
     WHEN testinstance adminkey, expecting failure, runs user add foo/bar foo@bar bananas
     THEN stderr contains user name .foo/bar. not valid

    FINALLY the instance is torn down
