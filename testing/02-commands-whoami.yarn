<!-- -*- markdown -*- -->

Who am I?
=========

A question which has long plagued philosophers is 'Who am I?' fortunately
Gitano can answer that, for users it knows about at least.

    SCENARIO user introspection

    GIVEN a standard instance
      AND testinstance, using adminkey, adds a new user alice, with a key called main
     WHEN alice main runs whoami

The output of `whoami` includes the user's name and email address

     THEN stdout contains User name: alice
      AND stdout contains Real name: alice's real name
      AND stdout contains Email address: alice@testinstance

Also it contains the list of ssh keys registered to the user.

    THEN stdout contains main

Also it contains a list of groups which the user is a member of.

    WHEN testinstance adminkey runs whoami
    THEN stdout contains gitano-admin

    FINALLY the instance is torn down

If the `whoami` command is being run over SSH then it marks the actively used
SSH key, for ease of understanding how the person came to be authenticated as
that user.

    SCENARIO whoami marks ssh key
    ASSUMING gitano is being accessed over ssh
    GIVEN a standard instance

    WHEN testinstance adminkey runs whoami
    THEN stdout contains adminkey \[\*\]

    FINALLY the instance is torn down
