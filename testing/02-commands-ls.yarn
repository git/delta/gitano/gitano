<!-- -*- markdown -*- -->

`ls [--verbose|-v] [<pattern>...]`
==================================

The `ls` command is one of the few which touch every repository in a Gitano
instance.  As such, it can take a while to run.  Theoretically it leaks the
number of Git repositories on the server by virtue of analysis of timing.

Basic operation
===============

Firstly, we verify the basic operation of ls, that as a gitano-admin we have
read access (at least) to everything and as such we can list all the
repositories.

    SCENARIO Basic operation of ls

    GIVEN a standard instance
     WHEN testinstance adminkey runs ls
     THEN stdout contains RW \ gitano-admin
      AND stderr is empty

    FINALLY the instance is torn down

General access control for ls
=============================

If you have no read or write access to a repository, it should not show up
when you run `ls`.

    SCENARIO No access means no show in ls

    GIVEN a standard instance
      AND testinstance has keys called other
     WHEN testinstance, using adminkey, adds user other, using testinstance other
      AND testinstance adminkey runs create stoat
      AND testinstance other runs ls
     THEN stdout does not contain stoat
      AND stderr is empty

    FINALLY the instance is torn down

Check listing of archived repositories
======================================

When a repository is marked as archived, it should not show up when you
run `ls` even if you have access, unless you pass `--all` to the ls command.

    SCENARIO Archived repositories do not show in ls

    GIVEN a standard instance
     WHEN testinstance adminkey runs ls
     THEN stdout contains gitano-admin
      AND stderr is empty
     WHEN testinstance adminkey runs config gitano-admin set project.archived true
      AND testinstance adminkey runs ls
     THEN stdout does not contain gitano-admin
      AND stderr is empty
     WHEN testinstance adminkey runs ls --all
     THEN stdout contains RWA gitano-admin
      AND stderr is empty

    FINALLY the instance is torn down

FIXME: Now that we have the ability to patch the rules we should add more tests
