-- jenkins-notify.post-receive.lua
--
-- Example post-receive global hook which notifies a Jenkins instance on
-- any and all refs updates (except refs/gitano/*) which happen.
--
-- It notifies Jenkins *before* passing the updates on to the project hook.
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
--
--
-- This is an example which is part of Gitano.
--

local project_hook, repo, updates = ...

local jenkinshost = "ci.gitano.org.uk"
local basepath = "/jenkins/git/notifyCommit?url="
local urlbase = "git://git.gitano.org.uk/"

local notify_jenkins = false

for ref in pairs(updates) do
   if not ref:match("^refs/gitano/") then
      notify_jenkins = true
   end
end

if notify_jenkins then
   log.state("Notifying Jenkins...")
   local code, msg, headers, content =
      http_get(jenkinshost, basepath .. urlbase .. repo.name .. ".git")
   if code ~= "200" then
      log.state("Notification failed somehow")
   end
   for line in content:gmatch("([^\r\n]*)\r?\n") do
      log.state("jenkins: " .. line)
   end
end

-- Finally, chain to the project hook
return project_hook(repo, updates)
