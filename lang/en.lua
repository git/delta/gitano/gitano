-- en.lua
--
-- English language pack base for Gitano (Basically en_GB)
--
-- Copyright 2016-2017 Daniel Silverstone <dsilvers@digital-scurf.org>
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions
-- are met:
-- 1. Redistributions of source code must retain the above copyright
--    notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright
--    notice, this list of conditions and the following disclaimer in the
--    documentation and/or other materials provided with the distribution.
-- 3. Neither the name of the author nor the names of their contributors
--    may be used to endorse or promote products derived from this software
--    without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
-- ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
-- FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
-- DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
-- OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
-- HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
-- LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
-- OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
-- SUCH DAMAGE.
--
--

return {
   -- The SETUP_ tokens are for the gitano-setup binary
   SETUP_USAGE = [[
usage: gitano-setup [<answers-file>...]

This tool creates the basic repository setup for a Gitano instance.

This is an interactive tool where if it has any questions for you, it will
visit each answers file in turn until it finds the answer.  This means
that for automation purposes you can specify multiple answers files with
the earlier ones overriding the later ones.

In summary, the behaviour is as follows:

The repository root is created if it does not exist, and a gitano-admin.git
repository is created within it.  Said repository is populated with the
example administration repository rules and an admin user and group.
]],
   SETUP_DEBUG_PARSING_ANSWERS = "Parsing answers file: ${file}",
   SETUP_DEBUG_COMBINE_ANSWERS = "Combining answers into conf...",
   SETUP_WELCOME = "Welcome to the Gitano setup process",
   SETUP_DO_CHECKS = "Performing system checks",
   SETUP_CHECK_SUPPLE = "... Checking supple sandboxing",
   SETUP_CHECK_SUPPLE_UNABLE = "... Supple unable to run trivial sandboxed code",
   SETUP_CHECK_SUPPLE_VALUES = "... Supple unable to pass trivial values in and out",
   SETUP_CHECK_SUPPLE_BIDIRECTIONAL = "... Supple unable to communicate bidirectionally",
   SETUP_CHECK_FAILED = "... Unable to run supple test",
   SETUP_CHECK_OK = "System checks out",
   SETUP_SETTING_IS = "Setting: ${key} is ${value}",
   SETUP_NOT_A_DIR = "${path}: not a directory",
   SETUP_ERROR_INVALID_NAME = "Invalid name: ${name}",
   SETUP_BATCH_MODE = "Batch mode engaged",
   SETUP_INTERACTIVE_MODE = "Interactive mode engaged",
   SETUP_STEP_1 = "Step 1: Determine everything",
   SETUP_PATHS_HOME_INFO = "Home directory for new Gitano user",
   SETUP_PATHS_SSH_INFO = "SSH directory for new Gitano user",
   SETUP_PATHS_PUBKEY_INFO = "Public key file for admin user",
   SETUP_PATHS_PUBKEY_NOT_FOUND = "Cannot find public key for admin user",
   SETUP_PATHS_BYPASSKEY_INFO = "Public key file for bypass user",
   SETUP_PATHS_BYPASSKEY_NOT_FOUND = "Cannot find public key for bypass user",
   SETUP_PATHS_REPOS_INFO = "Repository path for new Gitano instance",
   SETUP_ADMIN_USERNAME_INFO = "User name for admin user",
   SETUP_ADMIN_REALNAME_INFO = "Real name for admin user",
   SETUP_ADMIN_EMAIL_INFO = "Email address for admin user",
   SETUP_ADMIN_KEYNAME_INFO = "Key name for administrator",
   SETUP_SITE_NAME_INFO = "Site name",
   SETUP_LOG_PREFIX_INFO = "Site log prefix",
   SETUP_USE_HTPASSWD_INFO = "Store passwords with htpasswd? (needed for http authentication)",
   SETUP_PATHS_SKEL_INFO = "Path to skeleton gitano-admin content",
   SETUP_STEP_2 = "Step 2: Gather required content",
   SETUP_PREP_SITE_CONFIG = "=> Prepare site config",
   SETUP_ACQUIRE_SKEL = "=> Acquire skeleton gitano-admin",
   SETUP_PREP_BYPASS_USER = "=> Preparing bypass user (${user})",
   SETUP_BYPASS_USER_REAL_NAME = "Special site-wide rule/hook bypass user",
   SETUP_PREP_ADMIN_USER = "=> Preparing administration user (${user})",
   SETUP_PREP_GITANO_ADMIN = "=> Preparing gitano-admin group",
   SETUP_STEP_3 = "Step 3: Write out paths and gitano-admin.git",
   SETUP_MAKE_PATHS = "=> Make paths",
   SETUP_PREPARE_REPO = "=> Prepare repository",
   SETUP_PREPARE_FLAT_TREE = "=> Create a flattened tree",
   SETUP_PREPARE_COMMIT = "=> Commit that tree",
   SETUP_PREPARE_MASTER = "=> Attach that commit to master",
   SETUP_CHECK_ADMIN_REPO = "=> Ensure we can parse our resultant admin repository",
   SETUP_ADMIN_CONFIG = "=> Configure admin repo description and owner",
   SETUP_ERROR_NO_USER = "Could not find user",
   SETUP_ERROR_NOT_ADMIN = "User was not a gitano-admin",
   SETUP_ADMIN_REF = "=> Change the admin ref for gitano-admin.git",
   SETUP_WRITE_SSHKEYS = "=> Write the SSH authorized_keys file out",
   SETUP_COMPLETED = "Gitano setup completed",

   -- General errors used in many scripts
   ERROR_CANNOT_FIND_ADMIN_REPO = "Unable to locate administration repository.  Cannot continue",
   ERROR_BAD_ADMIN_REPO = "Unable to find the HEAD of the administration repository.  Cannot continue",
   ERROR_NO_COMMAND = "No command provided, cannot continue",
   ERROR_COMMAND_FAILED = "Error running command, exiting",
   ERROR_NOT_AUTHORISED = "Not authorised",
   ERROR_NO_ADMIN_REPO = "Unable to locate administration repository.  Cannot continue",
   ERROR_CANNOT_PARSE_ADMIN = "Unable to parse administration repository.",
   ERROR_CANNOT_CONTINUE = "Cannot continue",
   ERROR_CANNOT_LOCATE_REPO = "Unable to locate repository.",
   ERROR_REPO_IS_NASCENT = "Repository ${name} is nascent.",
   ERROR_NO_ERROR_FOUND = "No reason given, but errored somehow.",
   ERROR_RULESET_UNCLEAN_FINISH = "Ruleset did not complete cleanly.",
   ERROR_RULESET_DENIED_ACTION = "Ruleset denied action.  Sorry.",
   ERROR_ACTIONS_REFUSED_ACTION = "Builtin action handlers denied action.  Sorry",

   -- General messages which might be useful in various places
   FINISHED = "Finished",

   -- Messages from post-receive
   CHANGES_TO_ADMINREF_APPLIED = "<${name}> Any changes to admin ref have been applied",
   CHANGES_TO_HOOKS_APPLIED = "<${name}> Any changes to hooks etc have been applied",
   ERROR_UNABLE_TO_PROCESS = "<${name}> Unable to process: ${msg}",
   SCANNING_FOR_UPDATES = "Scanning repositories to apply hook/rules updates...",
   ALL_UPDATES_DONE = "All repositories updated where possible.",
   ERROR_UPDATE_SSH_NOT_WORK = "Updating SSH keys didn't work?",
   UPDATE_HTTP_INFO = "Updating server info for dumb HTTP transport",
   UPDATE_LASTMOD_DATE = "Updating last-modified date",
   RUNNING_POST_RECEIVE_HOOK = "Running repository post-receive hook",
   WARN_HEAD_DANGLING = "HEAD remains dangling",

   -- Messages from pre-receive
   RUNNING_PRE_RECEIVE_HOOK = "Running repository pre-receive hook",
   ODD_OLD_OBJECT_NOT_COMMIT_OR_TAG = "Odd, old object ${sha} is not a commit or tag",
   ODD_NEW_OBJECT_NOT_COMMIT_OR_TAG = "Odd, old object ${sha} is not a commit or tag",
   GENERATING_TREEDELTAS = "Treedeltas generating because of ${key}, please hold.",
   GENERATED_TREEDELTAS = "Treedeltas built, continuing.",
   ERROR_RULES_REFUSED_UPDATE = "Rules refused update: ${reason}",
   ERROR_BUILTIN_HANDLERS_SAID = "Builtin action handlers said: ${msg}",
   RUNNING_UPDATE_HOOK = "Running repository update hook",
   ALLOWING_UPDATE = "Allowing ref update of ${ref} from ${old} to ${new}",

   -- Messages from update-ssh
   UPDATE_SSH_USAGE = "Usage: gitano-update-ssh /path/to/repos",

   -- Messages from the actions module
   VERIFY_NEW_GITANO_ADMIN = "Verifying new gitano-admin master",
   LOOKS_OKAY = "Looks okay",
   VERIFY_NEW_ADMIN_SHA = "Verifying new admin sha: ${sha}",

   -- Messages from the auth module
   ERROR_PARSING_COMMAND = "Error parsing command",
   ERROR_COULD_NOT_LOAD_CONFIG = "Couldn't load a config from the admin repository",
   CLIENT_CONNECTED = "Client connected from ${ip} as ${user} (${key}), Executing command: ${cmdline}",
   ERROR_UNKNOWN_COMMAND = "Unknown command: ${cmd}",
   ERROR_VALIDATION_FAILED = "Validation of command line failed",
   ERROR_RULESET_UNCLEAN = "Ruleset did not complete cleanly",
   RULESET_ALLOWED = "Ruleset permitted action",
   RULESET_DENIED = "Ruleset denied action.  Sorry.",
   BYPASS_USER_BANNER_HEADER = "**** ALERT **** ALERT ****  PAY CAREFUL ATTENTION  **** ALERT **** ALERT ****",
   BYPASS_USER_ALERT_MESSAGE = "**** You are acting as the bypass user.  Rules and hooks WILL NOT APPLY  ****",
   BYPASS_USER_BANNER_FOOTER = "**** ALERT **** ALERT **** DO NOT DO THIS NORMALLY **** ALERT **** ALERT ****",
   PREAUTH_CMDLINE_HOOK_DECLINED = "Pre-authorization command line hook declined to permit action: ${reason}",
   PREAUTH_CMDLINE_HOOK_ABORTED = "Pre-authorization command line hook aborted: ${reason}",
   -- Messages from the config module
   NO_SITE_CONF = "No site.conf",
   NO_CORE_RULES = "No core rules file",
   ERROR_SITE_CONF_TYPE = "Error in gitano-admin:${sha}:site.conf [${key}] expected ${want} got ${got}",
   ERROR_DUPLICATE_USER_NAME = "Duplicate user name: ${name}",
   ERROR_USER_MISSING_REAL_NAME = "gitano-admin:${sha}:${path}/user.conf missing real_name",
   ERROR_USER_MISSING_EMAIL_ADDRESS = "gitano-admin:${sha}:${path}/user.conf missing email_address",
   ERROR_ORPHAN_KEY = "Found a key (${key}) for ${user} which lacks a user.conf",
   ERROR_BAD_KEY_NEWLINES = "Key ${filename} has newlines in it -- is it in the wrong format?",
   ERROR_BAD_KEY_SMELL = "Unable to parse key, ${filename} did not smell like an OpenSSH v2 key",
   ERROR_BAD_KEY_TYPE = "Unknown key type ${keytype} in ${filename}",
   ERROR_DUPLICATE_KEY = "Duplicate key found at (${keyname}) for ${username}.  Previously found as (${otherkey}) for ${otheruser}",
   ERROR_DUPLICATE_GROUP = "Duplicate group name: ${name}",
   ERROR_GROUP_NOT_BLOB = "${path}.conf is not a blob?",
   ERROR_GROUP_NO_DESCRIPTION = "${name}: No description?",
   ERROR_GROUP_NOT_FOUND = "Group ${name} not found, when traversing ${path}",
   ERROR_GROUP_LOOP = "Loop detected involving: ${path}",
   ERROR_DUPLICATE_KEYRING = "Duplicate keyring name: ${name}",
   ERROR_KEYRING_NOT_BLOB = "${path}.gpg is not a blob?",
   ERROR_NO_BYPASS_USER_FOUND = "User 'gitano-bypass' not found",
   ERROR_BYPASS_USER_IN_GROUP = "User 'gitano-bypass' is in group '${group}' which makes no sense",
   ERROR_BYPASS_USER_LACKING = "User 'gitano-bypass' has no SSH keys and does not have an HTTP password set",
   ERROR_GENERIC_CONF_NOT_FOUND = "Not found: ${sha}::${name}",
   ERROR_GENERIC_CONF_NOT_BLOB = "${sha}::${name}: Not a blob",
   DEBUG_ADDING_SSH_KEY = "Adding <${user}> <${key}>",
   ERROR_CANNOT_FIND_HOME = "Unable to find HOME",
   ERROR_UNABLE_TO_CREATE = "Unable to create ${path}",
   ERROR_UNABLE_TO_RENAME = "Unable to overwrite ${path}",
   AUTHORIZED_KEYS_UPDATED = "SSH authorised key file updated",

   -- Messages from the lace module
   DEBUG_EMPTY_OBJECT_NASCENT = "Returning empty object (nascent repository?)",
   ERROR_UNABLE_TO_FIND_RULE = "Unable to find $(name) in ${sha} (${inname})",
   ERROR_UNEXPECTED_ADDITIONAL_ARGUMENT = "Unexpected additional argument",
   ERROR_MISSING_MATCHTYPE_OR_VALUE = "Missing match type or value",
   ERROR_UNKNOWN_MATCHTYPE = "Unknown match type",
   LACE_BYPASS_BANNER_HEADER = "**** DANGER **** SOMETHING RISKY HAPPENING **** DANGER ****",
   LACE_BYPASS_ALERT_MESSAGE = "**** An ACL check was bypassed thanks to gitano-bypass ****",
   LACE_BYPASS_BANNER_FOOTER = "**** DANGER **** ACL BYPASS IS VERY RISKY  **** DANGER ****",
   LACE_BYPASSED = "ACL check bypassed because caller is gitano-bypass",

   -- Messages from the plugin module
   WARN_UNABLE_SCAN_PLUGINDIR = "Unable to scan plugin directory '${dir}': ${reason}",
   WARN_UNABLE_LOAD_PLUGIN = "Failure loading plugin '${plugin}' from '${file}': ${reason}",
   WARN_UNABLE_RUN_PLUGIN = "Failure running plugin '${plugin}' from '${file}': ${reason}",

   -- Messages from the repository module
   INITIAL_ADMIN_TREE = "Initial admin tree",
   ERROR_CANNOT_CREATE_EXPORT_FILE = "Can't create git-daemon-export file: ${reason}",
   ERROR_UNABLE_TO_RENAME_INTO_PLACE = "Unable to rename ${what} into place: ${reason}",
   ERROR_UNABLE_TO_WRITE_FILE = "Unable to write ${what} file: ${reason}",
   DEBUG_CHECKING_HOOKS = "Checking hooks for ${name}",
   ERROR_DELETED_ADMIN_REF = "A deleted admin ref cannot be loaded",
   ERROR_NO_PROJECT_CONF = "Unable to find project.conf in repository",
   WARN_MARKDOWN_ERROR = "<${name}> Markdown: ${reason}",
   ERROR_REALISE_NON_NASCENT = "Cannot realise a non-nascent repository",
   ERROR_CANNOT_PREP_PATH = "Cannot prepare path leading to repository",
   MSG_CHANGING_HEAD = "Changing project HEAD",
   MSG_CHANGED_HEAD = "<${name}> Set head to <${head}>",
   DEBUG_CALCULATE_TOKEN = "Calculating confirmation token for ${name}",
   ERROR_GRAVESITE_OCCUPIED = "Our grave site is already occupied! Cannot destroy.",
   ERROR_GRAVESITE_UNCHECKED = "Could not check grave site for occupancy. Cannot destroy.",
   ERROR_CANNOT_DESTROY_RENAME = "Cannot destroy.  rename() returned ${reason}",
   ERROR_CANNOT_RENAME_REPO = "Unable to rename repository: ${reason}",
   ERROR_TARGET_NOT_NASCENT = "Target repository is not nascent",
   ERROR_FAILED_TO_COPY = "Failed to copy repository ${path} to ${newpath}: ${reason}",
   ERROR_COPY_IN_PROGRESS = "Copy already in progress",
   ERROR_FAILED_COPYING_CONTENT = "Failed to copy repository content",
   ERROR_FAILED_HARDLINKING = "Failed to hardlink objects of ${path} to ${newpath}: ${reason}",
   ERROR_FAILED_RENAME_REPOSITORY = "Failed to rename repository ${from} to ${to}: ${reason}",
   ERROR_CANNOT_PREP_PATH_INFO = "Cannot repare path leading to info file.",
   ERROR_CANNOT_WRITE_LAST_MODIFIED = "Could not open info/web/last-modified for writing",
   MSG_UPDATED_ADMIN_TREE = "Updated admin tree",
   ERROR_SOMETHING_MISSING = "${what} missing",

   -- Messages from the supple module
   ERROR_NOT_RIGHT_NAME_FORMAT = "Unable to load ${modname} (Not the right name format)",
   ERROR_UNABLE_TO_LOAD_MODULE = "Unable to load ${modname}",
   DEBUG_HOOK_LOAD_MODULE = "Hook requested load_module(${modname})",
   DEBUG_ATTEMPT_LOADSTRING = "Attempting to loadstring in the sandbox",
   DEBUG_SET_GLOBALS = "Setting globals in supple sandbox",
   DEBUG_SUPPLE_HOST_RUN = "Entering supple.host.run()",
   SUPPLE_BYPASS_BANNER_HEADER = "**** PERIL **** A VERY HAZARDOUS OPERATION IS UNDERWAY **** PERIL ****",
   SUPPLE_BYPASS_ALERT_MESSAGE = "****  The running of a hook was bypassed thanks to gitano-bypass  ****",
   SUPPLE_BYPASS_BANNER_FOOTER = "**** PERIL **** HOOK BYPASS MAY CAUSE CRITICAL FAILURE **** PERIL ****",
   SUPPLE_BYPASSED = "Hook run bypassed because caller is gitano-bypass",

   -- Messages from the util module
   DEBUG_WELCOME_TO = "Welcome to ${site}",
   DEBUG_RUNNING = "Running:",
   DEBUG_ON_BEHALF_OF = "On behalf of ${user} using key ${key}",
   ERROR_RUNNING_COMMAND = "Error running ${cmd}: ${reason} ${code}",
   UNABLE_TO_CONTINUE = "Unable to continue",
   MSG_COMPLETED_SUCCESSFULLY = "${cmd} completed successfully",
   WARN_UNTERMINATED_STRING = "Un-terminated quoted string",
   WARN_UNUSED_ESCAPE = "Un-used escape at end",
   WARN_NO_COMMAND = "No command found?",
   ERROR_STAT_FILE_FAILED = "Stat file ${file} failed: ${reason}",
   ERROR_NO_CB = "No callback provided",
   MSG_SKIPPING_FILE = "Skipping file ${file}",
   MSG_COPYING_FILE = "Copying file ${from} to ${to}",
   ERROR_COPY_FILE_FAILED = "Copy file ${from} to ${to} failed: ${reason}",
   MSG_COPY_SYMLINK = "Copying symlink ${from} to ${to}",
   ERROR_COPY_SYMLINK_FAILED = "Copy symlink ${from} to ${to} failed: ${reason}",
   MSG_COPY_DIR = "Copying dir ${from} to ${to}",
   ERROR_COPY_DIR_FAILED = "Copy dir ${from} to ${to} failed: ${reason}",
   ERROR_UNSUPPORTED_TYPE = "Unsupported file type ${type} during copy",
   ERROR_EXPECTED_TAG_NAME = "Expected tag name in string expansion of ${...}",
   ERROR_UNTERMINATED_TAG = "Unterminated tag expansion in string.",
   ERROR_LOOP_IN_EXPN = "Loop detected in tag expansion",
}
